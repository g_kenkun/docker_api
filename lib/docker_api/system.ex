defmodule DockerApi.System do
  alias DockerApi.Connection

  @moduledoc """
  """
  @moduledoc since: "0.1.3"

  @doc """
  """
  @doc since: "0.1.3"

  def auth(opts \\ [], connection \\ %DockerApi.Connection{}) do
    path ="/auth"
    with {:ok, response} <- Connection.http_request(connection, :post, path, opts),
         %Tesla.Env{status: 200, body: body} <- response
      do
      {:ok, body}
    else
      {:error, any}
        -> {:error, any}
      %Tesla.Env{status: 204}
        -> :ok
      %Tesla.Env{status: status, body: %{"message" => message}}
        -> {:error, %{status: status, message: message}}
    end
  end

  @doc """
  https://docs.docker.com/engine/api/v1.40/#operation/SystemInfo

  ## Example

  ```elixir
  iex> DockerApi.System.info
  {:ok,
   %{
     "RuncCommit" => %{
       "Expected" => "3e425f80a8c931f88e6d94a8c831b9d5aa481657",
       "ID" => "3e425f80a8c931f88e6d94a8c831b9d5aa481657"
     },
   ...
   }}
  ```
  """
  @doc since: "0.1.3"

  def info(opts \\ [], connection \\ %DockerApi.Connection{}) do
    path = "/info"
    with {:ok, response} <- Connection.http_request(connection, :get, path, opts),
         %Tesla.Env{status: 200, body: body} <- response
      do
      {:ok, body}
    else
      {:error, any}
        -> {:error, any}
      %Tesla.Env{status: status, body: %{"message" => message}}
        -> {:error, %{status: status, message: message}}
    end
  end

  @doc """
  https://docs.docker.com/engine/api/v1.40/#operation/SystemVersion

  ## Example

  ```elixir
  iex> DockerApi.System.version()
  {:ok,
   %{
     "ApiVersion" => "1.40",
     "Arch" => "amd64",
     "BuildTime" => "2020-02-13T01:26:21.000000000+00:00",
     ...
   }}
  ```
  """
  @doc since: "0.1.3"

  def version(opts \\ [], connection \\ %DockerApi.Connection{}) do
    path = "/version"
    with {:ok, response} <- Connection.http_request(connection, :get, path, opts),
         %Tesla.Env{status: 200, body: body} <- response
      do
      {:ok, body}
    else
      {:error, any}
        -> {:error, any}
      %Tesla.Env{status: status, body: %{"message" => message}}
        -> {:error, %{status: status, message: message}}
    end
  end

  @doc """
  https://docs.docker.com/engine/api/v1.40/#operation/SystemPing

  ## Example

  ```elixir
  iex> DockerApi.System.ping()
  {:ok, "OK"}
  ```
  """

  @doc since: "0.1.3"

  def ping(opts \\ [], connection \\ %DockerApi.Connection{}) do
    path = "/_ping"
    with {:ok, response} <- Connection.http_request(connection, :get, path, opts),
         %Tesla.Env{status: 200, body: body} <- response
      do
      {:ok, body}
    else
      {:error, any}
      -> {:error, any}
      %Tesla.Env{status: status, body: %{"message" => message}}
      -> {:error, %{status: status, message: message}}
    end
  end

  @doc """
  https://docs.docker.com/engine/api/v1.40/#operation/SystemDataUsage

  ## Example

  ```elixir
  iex> DockerApi.System.usage_info()
  {:ok,
   %{
     "BuildCache" => nil,
     "BuilderSize" => 0,
     "Containers" => [
       %{
         "Command" => "/hello",
         "Created" => 1582206787,
         "HostConfig" => %{"NetworkMode" => "default"},
         "Id" => "b805952560348463423dd82e9e8767384d773c8db3d4689fd0f355e1f5a6e0a7",
         "Image" => "hello-world",
         "ImageID" => "sha256:fce289e99eb9bca977dae136fbe2a82b6b7d4c372474c9235adc1741675f587e",
         "Labels" => %{},
         "Mounts" => [],
         "Names" => ["/optimistic_hawking"],
         "NetworkSettings" => %{
           "Networks" => %{
             "bridge" => %{
               "Aliases" => nil,
               "DriverOpts" => nil,
               "EndpointID" => "",
               "Gateway" => "",
               "GlobalIPv6Address" => "",
               "GlobalIPv6PrefixLen" => 0,
               "IPAMConfig" => nil,
               "IPAddress" => "",
               "IPPrefixLen" => 0,
               "IPv6Gateway" => "",
               "Links" => nil,
               "MacAddress" => "",
               "NetworkID" => "27778b5eeb49566496aa150a1703c28cd9d22b3270c28c7da1ed7d566a4fc267"
             }
           }
         },
         "Ports" => [],
         "SizeRootFs" => 1840,
         "State" => "exited",
         "Status" => "Exited (0) 8 days ago"
       }
     ],
     "Images" => [
       %{
         "Containers" => 1,
         "Created" => 1546306167,
         "Id" => "sha256:fce289e99eb9bca977dae136fbe2a82b6b7d4c372474c9235adc1741675f587e",
         "Labels" => nil,
         "ParentId" => "",
         "RepoDigests" => ["hello-world@sha256:fc6a51919cfeb2e6763f62b6d9e8815acbf7cd2e476ea353743570610737b752"],
         "RepoTags" => ["hello-world:latest"],
         "SharedSize" => 0,
         "Size" => 1840,
         "VirtualSize" => 1840
       }
     ],
     "LayersSize" => 64207957,
     "Volumes" => []
   }}
  ```
  """
  @doc since: "0.1.3"

  def usage_info(opts \\ [], connection \\ %DockerApi.Connection{}) do
    path = "/system/df"
    with {:ok, response} <- Connection.http_request(connection, :get, path, opts),
         %Tesla.Env{status: 200, body: body} <- response
      do
      {:ok, body}
    else
      {:error, any}
      -> {:error, any}
      %Tesla.Env{status: status, body: %{"message" => message}}
      -> {:error, %{status: status, message: message}}
    end
  end
end
