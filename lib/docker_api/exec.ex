defmodule DockerApi.Exec do
  defstruct id: nil, connection: %DockerApi.Connection{}

  alias DockerApi.Connection

  @moduledoc false

  def create(container = %DockerApi.Container{}, opts) do
    path = path_for(container, :exec)
    Connection.http_request(container.connection, :post, path, opts)
  end

  def start(exec = %DockerApi.Exec{}, opts) do
    path = path_for(exec, :start)
    Connection.http_request(exec.connection, :post, path, opts)
  end

  def resize(exec = %DockerApi.Exec{}, opts) do
    path = path_for(exec, :resize)
    Connection.http_request(exec.connection, :post, path, opts)
  end

  def inspect(exec = %DockerApi.Exec{}, opts) do
    path = path_for(exec, :json)
    Connection.http_request(exec.connection, :get, path, opts)
  end

  defp path_for(exec = %DockerApi.Exec{}, path) do
    "/exec/#{exec.id}/#{path}"
  end

  defp path_for(container = %DockerApi.Container{}, path) do
    "/containers/#{container.id}/#{path}}"
  end
end
