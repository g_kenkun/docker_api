defmodule DockerApi do
  @moduledoc """
  Documentation for `DockerApi`.
  """

  @doc """
  Hello world.

  ## Examples

      iex> DockerApi.hello()
      :world

  """
  def hello do
    :world
  end
end
